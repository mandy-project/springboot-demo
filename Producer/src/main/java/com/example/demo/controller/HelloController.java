package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by jianyj on 2018/8/4.
 */
@RestController
public class HelloController {

    @GetMapping("/check/health")
    public Object health() {
        return "ok";
    }
}
