package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * Created by jianyj on 2018/8/4.
 */
@RestController
public class TestController {

    @Autowired
    private RestTemplate restTemplate;

    @GetMapping("/message")
    public Object message() {
        return getRestData();
    }

    public String getRestData(){
        return restTemplate.getForObject("http://eureka-client-consumer/check/health",String.class) ;
    }

}
